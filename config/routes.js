/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

// routes for product.

    'POST   /api/product': {                             // route for create product
        controller: 'ProductController',
        action: 'createProduct'
    },

    'GET    /api/product/': {                           // route for get product
        controller: 'ProductController',
        action: 'getProduct'
    },

    'GET    /api/product/byCategory/:categoryId': {     // route for list product by category
        controller: 'ProductController',
        action: 'getProductByCategory'
    },

    'GET    /api/product/byProductName/:productName': {  // route for serach product by product name
        controller: 'ProductController',
        action: 'getProductByProductName'
    },

    'PUT     /api/product/:id': {                        // route for update product
        controller: 'ProductController',
        action: 'updateProduct'
    },

    'DELETE     /api/product/:id': {                     // route for delete product
        controller: 'ProductController',
        action: 'deleteProduct'
    },

// routes for category.

    'POST   /api/category': {                            // route for create category
        controller: 'CategoryController',
        action: 'createCategory'
    },

    'GET    /api/category': {                            // route for get category
        controller: 'CategoryController',
        action: 'getCategory'
    },

    'GET    /api/category/byCategoryName/:categoryName': {  // route for search category by category name
        controller: 'CategoryController',
        action: 'getCategoryByCategoryName'
    },

    'PUT     /api/category/:id': {                       // route for update category
        controller: 'CategoryController',
        action: 'updateCategory'
    },

    'DELETE     /api/category/:id': {                    // route for delete category
        controller: 'CategoryController',
        action: 'deleteCategory'
    },
};
