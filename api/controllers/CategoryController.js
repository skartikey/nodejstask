/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    // Create category.

    createCategory: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        Category.create(req.body).exec(function (err, success) {

            if (err) {
                res.status(500);
                return res.json('500', { data: err });
            }

            res.json({ message: 'category data created', success })
        });

    },

    //Find category.

    getCategory: function (req, res) {
        Category.find().paginate({ page: req.query.page, limit: 10 })
            .exec(function (err, success) {
                if (err) {
                    res.status(500);
                    return res.json('500', { data: err });
                }
                res.json({ message: 'category list found', success })
            })
    },

    // Search category by name.

    getCategoryByCategoryName: function (req, res) {
        Category.find({ categoryName: req.params.categoryName })
            .paginate({ page: req.query.page, limit: 10 })
            .exec(function (err, success) {

                if (err) {
                    res.status(500);
                    return res.view('500', { data: err });
                }

                res.json({ message: 'category list found', success })
            })
    },

    //Update categroy.

    updateCategory: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        var params = {};

        var id = params.id;
        Category.find(id).exec(function (err, Category) {
            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }
            if (Category.length === 0) return res.send("No Category with that id exists.", 404);
            else {
                Category.update(id, params).exec(function (err, success) {

                    if (err) {
                        res.status(500);
                        return res.view('500', { data: err });
                    }
                    res.json({ message: 'category data updated', success })
                })
            }
        })
    },

    // Delete category.
    
    deleteCategory: function (req, res) {
        var id = req.param('id');
        Category.find(id).exec(function (err, Category) {
            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }
            if (Category.length === 0) return res.send("No Category with that id exists.", 404);
            else {
                Category.destroy({ id }).exec(function (err, success) {

                    if (err) {
                        res.status(500);
                        return res.view('500', { data: err });
                    }
                    res.json({ message: 'category data deleted' })
                })
            }
        })
    }

};

