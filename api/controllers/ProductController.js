/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    // Create product.

    createProduct: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        Product.create(params).exec(function (err, success) {

            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }

            res.json({ message: 'product data created', success })
        });

    },

    // Find product.

    getProduct: function (req, res) {
        Product.find().paginate({ page:req.query.page, limit: 10})
        .exec(function (err, success) {

            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }

            res.json({ message: 'product list found', success })
        })
    },

    // Find product by category.

    getProductByCategory: function (req, res) {
        Product.find({ categories: {contains: req.params.categoryId}})
        .paginate({ page:req.query.page, limit: 10})
        .exec(function (err, success) {

            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }

            res.json({ message: 'product list found', success })
        })
    },

    // Search product by name.

    getProductByProductName: function (req, res) {
        Product.find({ productName: req.params.productName})
        .paginate({ page:req.query.page, limit: 10})
        .exec(function (err, success) {

            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }

            res.json({ message: 'product list found', success })
        })
    },

    // Update product.

    updateProduct: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        var id = params.id;
        Product.find(id).exec(function (err, product) {
            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }
            if (product.length === 0) return res.send("No product with that id exists.", 404);
            else {
                Product.update(id, params).exec(function (err, success) {

                    if (err) {
                        res.status(500);
                        return res.view('500', { data: err });
                    }
                    res.json({ message: 'product data updated', success })
                })
            }
        })
    },

    // Delete product.

    deleteProduct: function (req, res) {
        var id = req.param('id');
        Product.find(id).exec(function (err, product) {
            if (err) {
                res.status(500);
                return res.view('500', { data: err });
            }
            if (product.length === 0) return res.send("No product with that id exists.", 404);
            else {
                Product.destroy(id).exec(function (err, success) {

                    if (err) {
                        res.status(500);
                        return res.view('500', { data: err });
                    }
                    res.json({ message: 'product data deleted' })
                })
            }
        })
    }

};

