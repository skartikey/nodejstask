/**
 * Product.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    productName: {
      type: 'string',
    },
    productDescription: {
      type: 'string',
    },
    image: {
      type: 'string',
    },
    status: {
      type: 'boolean',
    },
    categories: { type: 'json' },
  },

};

